#!/usr/bin/env python3

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

This script set new font on the system from /container/resources/fonts
"""

import os
import logging


class AddExtraFont:

    SOURCE_PATH = "/container/resources/fonts"
    DESTINATION_PATH = "/usr/share/fonts"

    def run(self):
        if os.path.exists(self.SOURCE_PATH):
            self.loop_over_direrctory(self.SOURCE_PATH)
            os.system("fc-cache")
        else:
            logging.warning("Fonts source path '%s' does not exists", self.SOURCE_PATH)

    def loop_over_direrctory(self, source_path):
        for element in os.listdir(source_path):
            src_path = os.path.join(source_path, element)
            dst_path = self.convert_source_to_destination_path(src_path)

            if os.path.exists(dst_path) and os.path.isdir(src_path):
                self.loop_over_direrctory(src_path)
            else:
                self.create_symbolic_link(src_path, dst_path)

    def convert_source_to_destination_path(self, path):
        return path.replace(self.SOURCE_PATH, self.DESTINATION_PATH)

    def create_symbolic_link(self, src_path, dst_path):
        cmd = f"ln -s {src_path} {dst_path}"
        os.system(cmd)


if __name__ == "__main__":
    add_extra_font = AddExtraFont()
    add_extra_font.run()
