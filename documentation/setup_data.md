# Setup data

The setup data is used to install data through csv and xml according the module installed version. It works with 2 scripts, the main script which manage the execution of the sub-scripts. Subscripts are contained in a directory that **must be** next to modules directory.

## The main script

The main script is [setup_data.py](../container/tools/setup_data.py). It's main purpose is to launch the sub-scripts. To lauch setup data simply call the main script with the 3 mandatory parameters:

- `-c/--config`  Path to odoo conf file
- `-d/--dir` Path of setup data directory which contain the sub-scripts
- `-m/--module` Module name wich will be use as module name for external ids

The main script proceed in 3 steps :

### 1. Create log table

To avoid launching several times a same sub-script, the main script log which sub-script was launch succefully in a table in the database `setup_data.log`. So the first thing it does is to check if this table is present in the database and if not then creates it.

### 2. Retrieve current module version

Sub-scripts directory **must be** next to modules directory, so the main script through is parameter -d/--dir will find the modules directory and loop over modules to find one that is installed in the table `ir_module_module`. The first installed module found will be the reference for the current installed version. This way of finding current install version supposed that all modules have the same version.

### 3. Launch sub-script

The sub-scripts directory must be organized like this. Directories named with the version number wich they refer. Those directories must contains a script named `setup_data.py` wich is their sub-script and optionally some data files.

```shell
subscripts_directory
├ 12.0.1.0.0
│  ├ setup_data.py
│  ├ some_data_file.xml
│  └ other_data_file.xml
├ 12.0.1.5.0
│  └ setup_data.py
└ 12.0.2.0.0
   ├ setup_data.py
   └ data
      ├ some_data_file.xml
      └ other_data_file.csv
```

Now that we have the log table and that we know the current installed version of the modules, we loop over the sub-scripts directory in ascending order of the version. Foreach version found we check in the log table if it has already been processed. If no we check if the version number is lower or equal to current installed version, if yes, we execute its sub-script `setup_data.py`.

If the subscript execution went well, its modification are committed to the database. If any unmanged error occured during the execution every modification made by this script are rollback and the main script stop.

## The sub-script

The sub-script is the script that really adds or modifies data in the database.

### How it works ?

It must be composed of a class object name `SetupData` that inherit from `AbstractSetupData` [abstract_setup_data.py](../container/tools/abstract_setup_data.py). This abstract class will bring an odoo environnement (`self.env`) and psycopg2 cursor (`self.cur`) and also 3 important methods :

- `_setup_data` : That must be reimplemented it's where you will write your code to install and modify data
- `_loading_data_file` : This method will take a relativ path from the sub-sript to an odoo data file (xml or csv)
- `_apply_settings` : Will take settings in form of a dictionnary to apply to `res.config.settings`

So if we take exemple of the 12.0.2.0.0 describe above we could have something like this:

```python
from abstract_setup_data import AbstractSetupData


class SetupData(AbstractSetupData):
    def _setup_data(self):

        self._loading_data_file("data/some_data_file.xml")
        self._loading_data_file("data/other_data_file.csv")

        self._settings_analytic()

    def _settings_analytic(self):

        settings = {
            "group_analytic_accounting": True,
            "group_analytic_tags": True,
        }

        self._apply_settings(settings, self.env.ref("base.main_company"))
```
