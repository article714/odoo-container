#!/bin/bash

# set -x

if [ "${1}" = "" ]; then
	echo "no tooling file provided"
else
	if [ -f ${1} ]; then
		source ${1}
	fi
fi

ODOO=$(type -p odoo)
if [ ! -x "$ODOO" ]; then
   echo "ERROR: cannot find Odoo CLI: $ODOO"
   exit 1
fi

ODOO_OPTS="--stop-after-init --without-demo=all --logfile=/var/log/odoo/odoo_build.log"

echo "-- Installing a new Odoo Database on ${odoo_host}, named ${db_name}"

#-------------------
# init db and install modules  (create DB & setup language)

echo "	Installing ODOO (create DB & setup Languages)"

$ODOO $ODOO_OPTS  --database=${db_name} -c ${ODOO_RC} -i base,web

PID_FILE=tmp/RUN/myodoo_${odoo_port}.pid

LANGUAGES="${language} ${additional_languages}"
for lang in $LANGUAGES; do
    $ODOO $ODOO_OPTS --database=${db_name} -c ${ODOO_RC} --load-language=${lang}
done

#-------------------
# init db and install modules

echo "	Installing ODOO modules"

export PGPASSWORD=${db_password}

for mod in  ${modules_to_install}; do
	$ODOO $ODOO_OPTS -i ${mod} --database=${db_name} -c ${ODOO_RC}
	if [ -d "ci/gen" ]; then
		cp -f /var/log/odoo/odoo_build.log ci/gen
	fi

	MODULE_INSTALLED=`psql -h ${db_host} -U ${db_username} -W ${db_name} -c "select name from ir_module_module where state='installed';" | grep -x " ${mod}"`
	if [ -z ${MODULE_INSTALLED} ]; then
		echo "failed to install ${mod}"
		if [ ! -d "ci/gen" ]; then
			cat /var/log/odoo/odoo_build.log
		fi
		exit 1
	else
		echo "module installed ${mod}"
	fi
done

rm -f $PID_FILE
