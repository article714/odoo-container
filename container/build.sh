#!/bin/bash

#set -x
# Generate French locales
localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8

export LANG=en_US.utf8
export DEBIAN_FRONTEND=noninteractive


# Adds contrib repo (fonts) [debian only, not ubuntu]
perl -pi -e "s/main$/main\ contrib/g" /etc/apt/sources.list

# Agree with Microsoft EULA license
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

# Install debian deps
apt-get update
apt-get upgrade -yq


if [ -f "/container/dependencies/debian_packages.txt" ]; then
  packages_to_install=$(cat /container/dependencies/debian_packages.txt)
  apt-get install -yq --no-install-recommends ${packages_to_install}
  if [ $? -ne 0 ]; then
      echo "ERROR: Failed to install common debian packages"
      exit 1
  fi
fi

if [ -f "/container/dependencies/${PRODUCT_VERSION}/debian_packages.txt" ]; then
  packages_to_install=$(cat /container/dependencies/${PRODUCT_VERSION}/debian_packages.txt)
  apt-get install -yq --no-install-recommends ${packages_to_install}
  if [ $? -ne 0 ]; then
      echo "ERROR: Failed to install version specific debian packages"
      exit 1
  fi
fi


# Install pip dependencies
python3 -m pip install -U pip
if [ $? -ne 0 ]; then
    echo "ERROR: Failed to upgrade PIP"
    exit 1
fi

if [ -f "/container/dependencies/requirements.txt" ]; then
    python3 -m pip install -r /container/dependencies/requirements.txt
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install python common dependencies"
        exit 1
    fi
fi

if [ -f "/container/dependencies/${PRODUCT_VERSION}/requirements.txt" ]; then
    python3 -m pip install -r /container/dependencies/${PRODUCT_VERSION}/requirements.txt
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install python version specidic dependencies"
        exit 1
    fi
fi

#retrieve article714/odootools

cd /container/tools
git clone --branch 12.0 --single-branch https://gitlab.com/article714/odootools.git


# update wkhtmltox (issue with default debian package)

cd /tmp/

BASE_WKHTML_VERSION=$(echo -n ${WKHTML_VERSION} | cut -d '-' -f 1)
DISTRO_NAME=$(python3 /container/tools/os-release.py -c )
if [ "${BASE_WKHTML_VERSION}" == "0.12.5" ]; then
    curl -o wkhtml.deb -sSL  https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${BASE_WKHTML_VERSION}/wkhtmltox_${WKHTML_VERSION}.${DISTRO_NAME}_amd64.deb
else
    curl -o wkhtml.deb -sSL  https://github.com/wkhtmltopdf/packaging/releases/download/${WKHTML_VERSION}/wkhtmltox_${WKHTML_VERSION}.${DISTRO_NAME}_amd64.deb
fi
if [ $? -ne 0 ]; then
    echo "ERROR: Failed to download WkHTML Version ${WKHTML_VERSION}"
    exit 1
fi

if [ -f "wkhtml.deb" ]; then
    dpkg -i --force-depends wkhtml.deb
    if [ $? -ne 0 ]; then
    echo "ERROR: Failed to install Wkhtml software"
        exit 1
    fi
    apt-get -yq install -f --no-install-recommends
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install Wkhtml Deps"
        exit 1
    fi
    rm -f wkhtml.deb
else
    echo "ERROR: Failed to download WkHTML Version ${WKHTML_VERSION}"
    exit 1
fi
cd

# install latest postgresql-client
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${DISTRO_NAME}-pgdg main" >/etc/apt/sources.list.d/pgdg.list
export GNUPGHOME="$(mktemp -d)"
repokey='B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8'
gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${repokey}"
gpg --armor --export "${repokey}" | apt-key add -
gpgconf --kill all
rm -rf "$GNUPGHOME"
apt-get update
if [ $? -ne 0 ]; then
    echo "ERROR: Failed to packages list, including postgresql"
    exit 1
fi
apt-get install -y postgresql-client
if [ $? -ne 0 ]; then
    echo "ERROR: Could not install postgresql client"
    exit 1
fi
rm -rf /var/lib/apt/lists/*

# Install rtlcss
npm install -g rtlcss

# ODOO user should have a > 1000 gid to ease uid/gid mapping in docker
addgroup --gid 1001 odoo

adduser --system --home /var/lib/odoo --gid 1001 --uid 1001 --quiet odoo
adduser odoo syslog

mkdir -p /home/odoo
chown -R odoo. /home/odoo
chmod -R 770 /home/odoo
chmod ug+s /home/odoo

# Install Odoo either from package or from source code

if [ ${WITH_DEBIAN_PYTHON_DEPS} -gt 0 ]; then
    curl -o odoo.deb -sSL http://nightly.odoo.com/${PRODUCT_VERSION}/nightly/deb/odoo_${PRODUCT_VERSION}.${ODOO_PACKAGE_RELEASE}_all.deb
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to download Odoo Version ${PRODUCT_VERSION}"
        exit 1
    fi
    if [ -f "odoo.deb" ]; then
       dpkg --force-depends -i odoo.deb
       if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install Odoo software"
           exit 1
       fi
       apt-get update
       apt-get -y install -f --no-install-recommends
       if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install Odoo software dependencies"
           exit 1
       fi
       rm -rf odoo.deb
    else
        echo "ERROR: Failed to download Odoo software"
        exit 1
    fi
else
    curl -o odoo.tar.gz -sSL http://nightly.odoo.com/${PRODUCT_VERSION}/nightly/src/odoo_${PRODUCT_VERSION}.${ODOO_PACKAGE_RELEASE}.tar.gz
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to download Odoo Version ${PRODUCT_VERSION}"
        exit 1
    fi
    if [ -f "odoo.tar.gz" ]; then
       cur_dir=$(pwd)
       tar -C /tmp  -xzf odoo.tar.gz
       src_dir=$(find /tmp -type d -iname "odoo-${PRODUCT_VERSION}*")
       cd ${src_dir}
       if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install Odoo software dependencies (PIP)"
           exit 1
       fi
       python3 setup.py install
       if [ $? -ne 0 ]; then
        echo "ERROR: Failed to install Odoo software from source"
           exit 1
       fi
       cd ${cur_dir}
       rm -rf  odoo.tar.gz
       rm -rf  ${src_dir}
    fi
fi

# install additional Odoo modules

mkdir -p /home/odoo/addons

if [ -f "/container/dependencies/modules_dependencies.txt" ]; then
    cp /container/dependencies/modules_dependencies.txt /home/odoo/addons
    cd /home/odoo/addons
    python3 /container/tools/clone_dependencies.py /home/odoo/addons ${PRODUCT_VERSION}
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to update dependencies for Odoo"
        exit 1
    fi
cd /
fi

if [ -f "/container/dependencies/${PRODUCT_VERSION}/modules_dependencies.txt" ]; then
    cp /container/dependencies/${PRODUCT_VERSION}/modules_dependencies.txt /home/odoo/addons
    cd /home/odoo/addons
    python3 /container/tools/clone_dependencies.py /home/odoo/addons ${PRODUCT_VERSION}
    if [ $? -ne 0 ]; then
        echo "ERROR: Failed to update dependencies for Odoo"
        exit 1
    fi

cd /
fi

# Copy Odoo configuration file
chgrp -R odoo /container/config/odoo

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
mkdir -p /mnt/extra-addons
chown -R odoo /mnt/extra-addons
chmod g+s /var/lib/odoo
mkdir -p /var/lib/odoo
chown -R odoo /var/lib/odoo
chmod ug+s /var/lib/odoo

# activate odoo and redis services
update-service --add /container/services/odoo
update-service --add /container/services/redis
update-service --add /container/services/redis_workers_pool

#--
# Cleaning

echo "INFO: starting removing useless data and packages"

python3 -m pip  cache purge

if [ -f "/container/dependencies/${PRODUCT_VERSION}/debian_packages_to_purge.txt" ]; then
  echo "INFO: Purging selected packages"
  packages_to_purge=$(cat /container/dependencies/${PRODUCT_VERSION}/debian_packages_to_purge.txt)
  apt-get purge -y ${packages_to_purge}
fi


if [ -f "/container/dependencies/debian_packages_to_purge.txt" ]; then
  echo "INFO: Purging selected packages"
  packages_to_purge=$(cat /container/dependencies/debian_packages_to_purge.txt)
  apt-get purge -y ${packages_to_purge}
fi

apt-get -yq clean
apt-get -yq autoremove
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/ci
rm -f tmp/*_dependencies.txt
rm -rf build
rm -rf /container/dependencies
