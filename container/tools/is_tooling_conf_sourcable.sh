echo "Check if tooling.conf is sourcable"

# This doesn't actually source it. It just test if source is working
errs=$(source "$TOOLING_CONF_PATH" 2>&1 >/dev/null)
retval=$?

if [ ${retval} = 0 ]; then
    # Do another check for any syntax error
    if [ -n "${errs}" ]; then
        echo "The tooling.conf file is not sourcable, it contains syntax errors : "
        echo "Error details:"
        printf "%s\n" "${errs}"
        exit 1
    fi
else
    echo "The tooling.conf file is not sourcable it returns an error code ${retval}: "
    echo "Error details:"
    printf "%s\n" "${errs}"
    exit 1
fi