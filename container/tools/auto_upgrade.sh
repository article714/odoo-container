#!/bin/bash

export TOOLING_CONF_PATH=`python3 /container/tools/check_tooling_conf.py`

source $TOOLING_CONF_PATH

#--------------------------------
# Remove specific services
update-service --remove /container/services/redis_workers_pool
update-service --remove /container/services/redis
update-service --remove /container/services/odoo

# Run other services
runsvdir -P ${SVDIR} &
sleep 2


#--------------------------------
# Upgrade Odoo
# TODO : Find another way to catch logs without syslog. Maybe with modification in the odoo.conf


chpst -u odoo odoo -d ${db_name} -c /container/config/odoo/odoo.conf -u all --no-http --stop-after-init

if [ $? != 0 ]; then

    if [ -f /container/auto_upgrade_priority.conf ]; then
        source /container/auto_upgrade_priority.conf
        chpst -u odoo odoo -d ${db_name} -c /container/config/odoo/odoo.conf -u ${priority_module} --no-http --stop-after-init
    fi

    chpst -u odoo odoo -d ${db_name} -c /container/config/odoo/odoo.conf -u all --no-http --stop-after-init

    if [ $? != 0 ]; then
        exit 1
    fi
fi
#--------------------------------
# Update Data
# TODO : Find another way to catch logs without syslog. Maybe with modification in the odoo.conf

chpst -u odoo python3 /container/tools/setup_data.py -c $TOOLING_CONF_PATH --dir=${setup_data_dir} -m lease4_data

#--------------------------------
# Kill services
kill -HUP $(pidof runsvdir)

while [ ! -z "$(pidof runsvdir)" ]
do
    sleep 2
done

#--------------------------------
# Remove specific services
# Warning : Services are started in the order of addition
update-service --add /container/services/odoo
update-service --add /container/services/redis
update-service --add /container/services/redis_workers_pool
