# Tooling conf

## What is tooling conf ?

Tooling conf is a conf file used for tools such as setup_data, autoupgrade build_odoo_db.
It contains various information such as :

- Connection param to database
- Odoo server information
- Localization
- setup_data configuration

## check_tooling_conf.py

[check_tooling_conf.py](../tools/check_tooling_conf.py) has for purpose to retrieve the path of the tooling conf file and check it's content.
Odootools scripts look for this file in two default location. First `/container/config/odoo/tooling.conf` and if nothing is found `/container/config/tooling/tooling.conf`

It can be used in bash script like this :

```bash
export TOOLING_CONF_PATH=`python3 /container/tools/check_tooling_conf.py`
```
