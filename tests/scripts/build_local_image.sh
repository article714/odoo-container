#!/bin/bash

# set -x

is_wsl=$(uname -a | grep microsoft | grep -c WSL)

# versions management
PRODUCT_VERSION="12.0"
ODOO_PACKAGE_RELEASE=latest
BASE_DEBIAN_VERSION="python-3.9"
DEBIAN_CONTAINER_VERSION="0.10.0"
WKHTML_VERSION="0.12.6.1-2"
MIN_PYTHON_VERSION="3.10.6"
WITH_DEBIAN_PYTHON_DEPS=0
POSTGRESQL_IMAGE="postgres:14"

# local test image identification
IMAGE_VERSION="localtest"
IMAGE_NAME=odoo-container
CI_COMMIT_BRANCH="__local__"

# Docker build opts
#BUILD_OPTS='--force-rm --no-cache'
BUILD_OPTS=''
DOCKER_ADDTL_PARAMS="--build-arg PRODUCT_VERSION=${PRODUCT_VERSION}
                     --build-arg A714_COMMIT_TAG=${CI_COMMIT_BRANCH}
                     --build-arg WKHTML_VERSION=${WKHTML_VERSION}
                     --build-arg BASE_DEBIAN_VERSION=${BASE_DEBIAN_VERSION}
                     --build-arg DEBIAN_CONTAINER_VERSION=${DEBIAN_CONTAINER_VERSION}
                     --build-arg WITH_DEBIAN_PYTHON_DEPS=${WITH_DEBIAN_PYTHON_DEPS}
                     --build-arg ODOO_PACKAGE_RELEASE=${ODOO_PACKAGE_RELEASE}
                     "


#---------------
# Let's build image

echo " Building ${IMAGE_NAME}:${IMAGE_VERSION}"
docker build ${BUILD_OPTS} -t article714/${IMAGE_NAME}:${IMAGE_VERSION} --build-arg IMAGE_VERSION=${IMAGE_VERSION} ${DOCKER_ADDTL_PARAMS} .
