# odoo-container

An [Odoo](https://odoo.ccom) container tailored to Article714 instrastucture needs.

It makes use of [debian-based-container](https://gitlab.com/article714/debian-based-container) as a base image to build a container with several services:

- odoo
- redis
- runit
- rsyslog

The following odoo repositories from the OCA are included into this distribution:

- queue
- reporting-engine
- server-auth
- server-env
- server-tools
- web

The following odoo repositories from the communuity (OCA excepted) are also included into this distribution:

- odoo-cloud-platform [from CamptoCamp](https://github.com/camptocamp/odoo-cloud-platform.git), [forked by Tekfor](https://github.com/Tekfor-cloud/odoo-cloud-platform.git)

Every dependency listed in those repos (python or odoo modules) are also installed into image. Thus any Odoo module available in those OCA repositories is installable, on any Odoo instance built from this image.

## Branching policy

Three main branches are used to develop `odoo-container`:

- **master**, main development branch, used for publishing _:preview_ docker images
- **production**, used for publishing _:latest_ docker images
- **test**, used for developments requiring to skip certain phases of the build

Official releases (except _:latest_) are produced from a ** tag ** in the format _vX.Y.Z_.

**WARNING** This repository changed its organization several times. Thus it contains several obsolete branches like:

- `<odoo version>_dev`, were used to work on new features and build _preview_ images, prior to `*.0.8` versions;
- `<odoo version>`, were used to stabilize version and build _latest_ images for a given version of Odoo, prior to `*.0.8` versions.

Le last (and current) organization is back to a more standard way of doing things, and makes use of [Gitlab CI parallel feature](https://docs.gitlab.com/ee/ci/yaml/index.html#parallel) to
build image for all supporterd Odoo versions at the same time.

## Versioning

Since `22.10.0` we opted for a versioning scheme that includes year and month of release with the following structure `YY.MM.Rev`:

- `MM`, month of release,
- `YY`, last 2 digits of release year
- `Rev`, minor revision of container (should not add features)

Building Odoo container uses the same process for all supported versions of Odoo, and we try to keep it idempotent whatever the targer Odoo version is (i.e. with the same Odoo module included).

## Building a new version

Two versions _kind_ are built for the project:

- _preview_ versions are built from `<odoo version>_dev` branches (e.g. `12.0_dev` will publish `article714/odoo-container:12.0-preview` image),
- _latest_ versions are built from `<odoo version>` branches (e.g. `12.0_dev` will publish `article714/odoo-container:12.0-latest` image),
- releases are built from tags formated as: `X.X.Y`, where `X.X` represents Odoo version (e.g. `12.0`), and `Y` is image revsion number.

The container image will be built on dedicated gitlab-runner and pushed on [hub.docker.com](https://hub.docker.com/r/article714/odoo-container).

To _prevent image to be pushed_ to docker public registry, you can setup a [Gitlab CI/CD project variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project): `DO_NOT_PUBLISH`. If set (whatever the value) it will skip image publishing steps.

To add a new supported Odoo version, you will need to add an entry to `.project_versions` entry in `.gitlab-ci.yml`file:

```yaml
.project_versions:
  parallel:
    matrix:
      - PRODUCT_VERSION: "" # Base Odoo version (e.g. 12.0 or 14.0)
        ODOO_PACKAGE_RELEASE: latest # wich odoo package to use, if you need to use a specific revision for .deb file
        BASE_DEBIAN_VERSION: "ubuntu-20.04" # debian-based-container flavor to use
        DEBIAN_CONTAINER_VERSION: "0.11.0" # debian-based-container version to use
        WKHTML_VERSION: "0.12.6-1" # WkHTML version that will be installed
        MIN_PYTHON_VERSION: "3.8" # Minimum python version (used to verify compatibility)
        WITH_DEBIAN_PYTHON_DEPS: 1 # Version can be built for source code or using deb packages, depending on the value of `WITH_DEBIAN_PYTHON_DEPS`
        POSTGRESQL_IMAGE: postgres:11 # Which postgresql container image to use for tests
```

## Services embedded

### **odoo**

(ToDo)

### **redis**

(ToDo)

### **redis workers pool**

This service allows you to create pools of redis queue workers ([redis queue documentation](https://python-rq.org/)) that are able to interact with your odoo instance.

For more information see redis workers pool [documentation](container/services/redis_workers_pool/README.md)