#!/usr/bin/env python3
"""
©2023 TekFor
License: L-GPL v3

@authors: C. Guychard (Tekfor)
          D. Couppé (Apside)


This script tests if db_name defined in odoo.conf exists
on postgresql server defined in same config file
"""


from os import environ
from sys import exit as sysexit
import logging


from odoocommons import read_odoo_conf, get_odoo_option
import psycopg2


def main() -> None:
    odoo_conf = read_odoo_conf()

    db_host = get_odoo_option(odoo_conf, "db_host")
    db_port = get_odoo_option(odoo_conf, "db_port", optiontype="intorfalse")
    db_user = get_odoo_option(odoo_conf, "db_user")
    db_password = get_odoo_option(odoo_conf, "db_password")
    db_name = get_odoo_option(odoo_conf, "db_name")

    if db_port is None or not db_port or db_port == "False":
        db_port = 5432

    try:
        if db_host:
            psycopg2.connect(
                dbname=db_name,
                user=db_user,
                host=db_host,
                password=db_password,
                port=db_port,
            )
        else:
            psycopg2.connect(dbname=db_name, user=db_user, password=db_password)
    except psycopg2.Error as exc:
        logging.error(
            f"Could not connect to PG Database {db_name} using {db_host}:{db_port} and user: {db_user}"
        )
        logging.error(f"psycopg exception said: {type(exc)} - {exc.args}")
        sysexit(1)

    sysexit(0)


if __name__ == "__main__":
    main()
