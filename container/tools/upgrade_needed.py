#!/usr/bin/env python3

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

The script check module version in database and in the manifest file
and update the module if the version number in the file is higher than the number in database
The odoo.conf path must be setted on the environment variable ODOO_RC
"""

import os
import sys
from configparser import ConfigParser
import logging

import psycopg2
import psycopg2.extras

from odoo import modules

logging.basicConfig(level=logging.INFO)


def is_upgrade_needed(conf_path: str) -> bool:
    config = read_config(conf_path)

    installed_versions = list_installed_module_in_db(config)
    file_versions = list_files_version(config)

    need_upgrade = False
    for module, db_version in installed_versions.items():
        file_version = file_versions.get(module)
        if file_version is None:
            logging.warning(
                " Module %s is installed in version %s, but module can't be found in any path of addons_path",
                module,
                db_version,
            )
        elif file_version != db_version:
            logging.info(
                " Module %s => db_version : %s file_version : %s",
                module,
                db_version,
                file_version,
            )
            if is_file_version_higher_than_db_version(file_version, db_version):
                need_upgrade = True
            else:
                logging.error(
                    " Module %s : Installed version is higher than file version", module
                )
                sys.exit(1)
    return need_upgrade


def read_config(conf_path) -> ConfigParser:
    config = ConfigParser()
    config.read(conf_path)

    return config


def list_files_version(config) -> list:
    manifests = []
    for addon_path in config.get("options", "addons_path").split(","):
        manifests.extend(list_manifest_from_tree(addon_path))

    files = {}
    for manifest in manifests:
        files[
            extract_module_name_from_manifest_path(manifest)
        ] = extract_module_version_from_manifest_path(manifest)

    return files


def list_manifest_from_tree(path) -> list:
    manifests = []
    for walk_result in os.walk(path):
        current_path = walk_result[0]
        files = walk_result[2]
        if "__manifest__.py" in files:
            manifests.append(os.path.join(current_path, "__manifest__.py"))
    return manifests


def extract_module_version_from_manifest_path(manifest_path) -> str:
    with open(manifest_path, encoding="utf-8") as f:
        manifest = eval(f.read())
    return modules.adapt_version(manifest.get("version", "1.0"))


def extract_module_name_from_manifest_path(manifest_path) -> str:
    return os.path.split(os.path.dirname(manifest_path))[1]


def list_installed_module_in_db(config: ConfigParser) -> dict:

    with psycopg2.connect(**get_connection_param(config)) as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.execute(
                """
                SELECT name, latest_version
                FROM public.ir_module_module
                WHERE state='installed'
                """
            )
            return dict(cur.fetchall())


def get_connection_param(config: ConfigParser) -> dict:
    def option_get(option_name: str):
        option = config.get("options", option_name)
        if option == "False":
            option = False
        return option

    db_host = option_get("db_host")
    db_port = option_get("db_port")
    db_user = option_get("db_user")
    db_password = option_get("db_password")
    db_name = option_get("db_name")

    return {
        "dbname": db_name,
        "user": db_user,
        "password": db_password,
        "host": db_host,
        "port": db_port or 5432,
    }


def is_file_version_higher_than_db_version(file_version, db_version) -> bool:
    result = True
    for file, db in zip(file_version.split("."), db_version.split(".")):
        if int(file) > int(db):
            break
    else:
        result = False

    return result


if __name__ == "__main__":

    if len(sys.argv) == 1:
        CONF_PATH = os.environ["ODOO_RC"]
    else:
        CONF_PATH = sys.argv[1]

    if is_upgrade_needed(CONF_PATH):
        sys.exit(2)
    else:
        sys.exit(0)
