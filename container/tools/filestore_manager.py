# coding: utf-8

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

"""

import os
import stat
import zipfile

import odoo
from odootools.src.odootools.odooscript import AbstractOdooScript


class FilestoreManager(AbstractOdooScript):
    AbstractOdooScript.getopt_options += "d:o:"
    AbstractOdooScript.getopt_long_options.extend(("dir=", "operation="))

    def get_help_message(self):
        """
        build help message string
        """
        help_message = AbstractOdooScript.get_help_message(self)
        help_message += "-d, --dir\t\t[REQUIRED] Path to dump or restor dir\n"
        help_message += "-o, --operation\t\t[REQUIRED] Operation to do. Must have value: dump or restore\n"
        return help_message

    def run(self, cur, env):
        operation = self.get_option("-o", "--operation")
        dbname = cur.connection.get_dsn_parameters()["dbname"]
        directory = self.get_option("-d", "--dir")
        dump_file = os.path.join(directory, "dump_filestore_{}".format(dbname))
        filestore = odoo.tools.config.filestore(dbname)

        if operation == "dump":
            self.dump(filestore, directory, dump_file)
        elif operation == "restore":
            self.restore(dump_file, filestore)
        else:
            self.print_help()

    def dump(self, filestore, directory, dump_file):

        if os.path.exists(filestore):
            if not os.path.exists(directory):
                os.makedirs(directory)
                os.chmod(
                    directory,
                    stat.S_IRUSR
                    | stat.S_IWUSR
                    | stat.S_IXUSR
                    | stat.S_IRGRP
                    | stat.S_IWGRP
                    | stat.S_IXGRP,
                )
            self.logger.info("Dumping filestore from %s to %s", filestore, dump_file)
            odoo.tools.osutil.zip_dir(filestore, dump_file, include_dir=False)
        else:
            self.logger.warning("Nothing to do! There is no filestore at %s", filestore)

    def restore(self, dump_file, filestore):

        with zipfile.ZipFile(dump_file, "r") as z:
            self.logger.info("Restoring filestore from %s to %s", dump_file, filestore)
            z.extractall(filestore)


if __name__ == "__main__":

    FILESTORE_MANAGER = FilestoreManager()
    FILESTORE_MANAGER.run_in_odoo_context()
