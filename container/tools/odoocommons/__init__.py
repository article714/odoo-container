
from os import environ
from configparser import ConfigParser,  NoSectionError, NoOptionError
import logging


def read_odoo_conf() -> ConfigParser:

    odoo_conf = ConfigParser()

    file_path = environ.get("ODOO_RC", "None")
    if not file_path:
        logging.error(
            f"You need to define ODOO_RC environment var pointing to odoo.conf")
    else:
        result = odoo_conf.read(file_path)

        if not result:
            logging.error(f"Could not parse file: {file_path}")

    return odoo_conf


def write_odoo_conf(odoo_conf: ConfigParser) -> None:
    if "ODOO_RC" not in environ:
        logging.error(
            f"You need to define ODOO_RC environment var pointing to odoo.conf")

    with open(environ["ODOO_RC"], "w") as odoo_conf_file:
        odoo_conf.write(odoo_conf_file)


def get_odoo_option(odoo_conf, optioname: str, optiontype: str = "str") -> str:
    try:
        if optiontype == "str":
            return odoo_conf.get("options", optioname)
        elif optiontype == "bool":
            return odoo_conf.getboolean("options", optioname)
        elif optiontype == "int":
            return odoo_conf.getint("options", optioname)
        elif optiontype == "intorfalse":
            try:
                return odoo_conf.getint("options", optioname)
            except ValueError:
                return odoo_conf.getboolean("options", optioname)

    except NoSectionError:
        logging.error("Could not parse odoo.conf")
        return None
    except NoOptionError:
        return None
