# Redis workers pool

This service allows you to create pools of redis queue workers ([redis queue documentation](https://python-rq.org/)) that are able to interact with your odoo instance. Every function enqueued in those queue will receive 2 special arguments (via kwargs):

- `cur` : which is the psycopg2 cursor of the current database transaction
- `env` : which is the current odoo environement

## Configuration

This service rely on 2 configurations files. The first one is for the odoo environnement which is the file held on `$ODOO_RC` and the second one is for the service itself and is by default `/container/config/redis_workers_pool/workers.yaml`

### workers.yaml

It can be empty and so the service will start with all the default value.

Available parameters :

- `extra_python_path` : A PYTHONPATH like string to add to PYTHONPATH

  - *default* : None

  ```yaml
  extra_python_path: "/path/to/directory"
  ```

- `redis` : Redis server connection

  - *default port* : 6380
  - *default host* : localhost

  ```yaml
  redis:
    port: 6380
    host: localhost
  ```

- `logging` : A Path to python logging module conf file

  - *default* : /container/config/redis_workers_pool/workers-logging.json

  ```yaml
  logging:
    configfile: /container/config/redis_workers_pool/workers-logging.json
  ```

- `pools` : Pools configuration

  It's structured as follow :

  ```yaml
  pools:
    *pool_name*:
      intances: *number of workers in pool*
      queue_name: *name of the redis queue*
  ```

  - *default pool_name* : default
  - *default intances* : 1
  - *default queue_name* : default
