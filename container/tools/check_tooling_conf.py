#!/usr/bin/env python3

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

Set of script used to read tooling configuration files and check his data
"""


import os
from configparser import ConfigParser


class NoValidPath(Exception):
    pass


class InvalidConfValue(Exception):
    pass


class MissingVar(Exception):
    pass


def main():
    tooling_conf_path = get_tooling_conf_path()
    config = read_tooling_conf(tooling_conf_path)
    check_tooling_conf_content(config)
    print(tooling_conf_path)


def get_tooling_conf_path() -> str:
    possible_paths = (
        "/container/config/odoo/tooling.conf",
        "/container/config/tooling/tooling.conf",
    )
    for path in possible_paths:
        if os.path.exists(path):
            return path

    raise NoValidPath("No valid path found")


def read_tooling_conf(tooling_conf_path: str) -> ConfigParser:
    with open(tooling_conf_path) as f:
        config_string = f.read()
    config = ConfigParser()
    config.read_string("[options]\n" + config_string)

    return config


def check_tooling_conf_content(config: ConfigParser):
    check_langage(config)
    check_timezone(config)
    check_db_conf(config)
    check_setup_data_dir(config)


def check_langage(config: ConfigParser):
    accepted_value = ("fr_FR", "en_US", "es_ES", "pt_PT")
    langage = config.get("options", "language").strip('"')
    if langage not in accepted_value:
        raise InvalidConfValue(
            f"langage value '{langage}' is not supported. Possible value are: {', '.join(accepted_value)}"
        )


def check_timezone(config: ConfigParser):
    accepted_value = ("Europe/Paris",)
    timezone = config.get("options", "timezone").strip('"')
    if timezone not in accepted_value:
        raise InvalidConfValue(
            f"timezone value '{timezone}' is not supported. Possible values are: {', '.join(accepted_value)}"
        )


def check_db_conf(config: ConfigParser):
    params = ("db_host", "db_name", "db_username", "db_password")
    for param in params:
        if not config.get("options", param):
            raise MissingVar("Missing variable: {param}")


def check_setup_data_dir(config: ConfigParser):
    value = config.get("options", "setup_data_dir")
    if not config.get("options", "setup_data_dir"):
        raise MissingVar("Missing variable: setup_data_dir")
    if not os.path.exists(value):
        raise InvalidConfValue(f"setup_data_dir value '{value}' is not a valid path")


if __name__ == "__main__":
    main()
