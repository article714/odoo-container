# Auto-Upgrade

Auto-upgrade is a system that allows the container on start to upgrade its database if needed.
It relies on 2 tools, [upgrade_needed.py](../container/tools/upgrade_needed.py) to detect if an upgrade is needed and [auto_upgrade.sh](../container/tools/auto_upgrade.sh) that does the upgrade and finally one mode [autoupgrade.yaml](../container/modes/autoupgrade.yaml) that will manage everything.

See more info on modes in debian-based-container documentation [here](https://gitlab.com/article714/debian-based-container/-/blob/master/documentation/state_and_mode_mgmt.md)

## Is upgrade needed ?

To know if an upgrade is needed the script [upgrade_needed.py](../container/tools/upgrade_needed.py) will first connect with psycopg2 to the database (define in the conf file set in $ODOO_RC or in the conf file passed in param when the script is call) to retrieve all installed modules and their version number in the table     `public.ir_module_module`.

Then the script will walkthrough all paths defined in `addons_path` in the conf file and collect all `__manifest__.py` files that it can found and track, for each file found, module name and version.

Finally it will match module name from database with module name from files and compare versions. We could have 4 cases.

| Cases                                 | Results                                                                                               |
| :------------------------------------ | :---------------------------------------------------------------------------------------------------- |
| db_version = file_version             | **no** upgrade needed                                                                                 |
| db_version < file_version             | **upgrade needed**                                                                                    |
| db_version but no matching file found | Warning in log and **no** upgrade needed                                                              |
| db_version > file_version             | **Error** (In this case the script crash to prevent database corruption with an older version of code) |

## Auto-upgrading

The auto upgrading tool [auto_upgrade.sh](../container/tools/auto_upgrade.sh) works with the tooling conf file retrieve by the tool [check_tooling_conf.py](../container/tools/check_tooling_conf.py). The script does nothing much that calls with the odoo command line tool. It works like this:

- Preparing to upgrade
  - Retreiving tooling conf
  - Remove services related to odoo (such as odoo, redis_workers_pool...)
  - Launch runsv to launch syslog
- Upgrading Odoo
  - First try to run `odoo -u all` to update odoo database according to files
  - If it failed try to find /container/auto_upgrade_priority.conf (It will be detailed later) and run `odoo -u ${priority_module}`
  - If previous step append and succeed, then second and last try to run `odoo -u all`
- Installing new data file
  - Running [setup_data.py](../container/tools/setup_data.py) tool
- Ending
  - Killing runsv
  - Re-add services related to odoo

### auto_upgrade_priority.conf

This file has to be here `/container/auto_upgrade_priority.conf` and must contain one variable wich is `priority_module` with a value compatible with the parameter `-u` of odoo command line.

This conf file will be used only if upgrading all modules at once failed. It's useful only if you detect that upgrading all modules according to dependencies order will fail and that you have to upgrade some modules before other no matter the dependencies order. So you specifie this modules in this conf file.

```bash
priority_module = account,stock,...
```

## auto upgrade mode

The auto upgrade mode defined in [autoupgrade.yaml](../container/modes/autoupgrade.yaml) will manage the auto-upgrade process in 5 steps.

- Check if database exists
- Call tool [add_extra_fonts.py](../container/tools/add_extra_fonts.py)
- Check presence of the flag file `/container/config/auto_upgrade_on.flg` if this file is note present the following steps are not executed
- Check if upgrade is needed with [upgrade_needed.py](../container/tools/upgrade_needed.py)
- Execute [auto_upgrade.sh](../container/tools/auto_upgrade.sh) if needed
