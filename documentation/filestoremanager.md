# Filestore manager

As it's name suggests, it's a tool to manage your filestore and more precisely to dump it and restore it, in the form of a zip file. It's useful if you want to restore a database for example, to have it works completely you have to restore its filestore too.

## Use it

It must be called whith 3 parameters :

- -c/--config Path to Odoo conf file or tooling conf file. It must contains `dbname` for which we dump or restore the filestore
- -d/--dir Directory from where to find the zipped filestore (or where to create it)
- -o/--operation it can take to value `restore` or `dump`

Dumping filestore :

```shell
filestore_manager -c path/to/conf.conf -o dump -d destination/directory
```

The result of the dumping command is a file in the destination directory called `dump_filestore_{dbname}`

Restoring filestore :

```shell
filestore_manager -c path/to/conf.conf -o restore -d source/directory
```

The file `dump_filestore_{dbname}` must be present in the source directory.
