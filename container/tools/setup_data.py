#!/usr/bin/env python3

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

This script set data in odoo with a specific module name using XML and CSV files
"""

import os
from importlib.machinery import SourceFileLoader
from datetime import datetime

import odoo.tools as tools

from odootools.src.odootools.odooscript import AbstractOdooScript


class SetupData(AbstractOdooScript):
    AbstractOdooScript.getopt_options += "d:m:"
    AbstractOdooScript.getopt_long_options.extend(("dir=", "module="))

    def get_help_message(self):
        """
        build help message string
        """
        help_message = AbstractOdooScript.get_help_message(self)
        help_message += "-d, --dir\t\t[REQUIRED] path of setup data directory\n"
        help_message += "-m, --module\t\t[REQUIRED] module name\n"
        return help_message

    def run(self, cur, env):
        self.env = env
        self.cur = cur

        self.init_setup_data_table()

        self.process_version_dir()

    def init_setup_data_table(self):
        created = self.create_setup_data_db_schema()
        self.create_setup_data_log_db_table(force_create=created)

    def create_setup_data_db_schema(self):
        created = False
        with self.cur.connection.cursor() as cur:
            # Test si le schema de migration existe en base
            cur.execute(
                """
                SELECT
                    exists(
                        SELECT 1 FROM pg_catalog.pg_namespace WHERE nspname = 'setup_data'
                    );
                """
            )

            if not cur.fetchone()[0]:
                self.logger.info("Create setup_data schema")
                cur.execute("CREATE SCHEMA setup_data;")
                created = True
        return created

    def create_setup_data_log_db_table(self, force_create=False):
        with self.cur.connection.cursor() as cur:
            if not force_create:
                cur.execute(
                    """
                        SELECT
                        exists(
                            SELECT * FROM pg_catalog.pg_tables
                            WHERE schemaname = 'setup_data' and tablename='log'
                        );"""
                )

                need_create = not cur.fetchone()[0]
            else:
                need_create = True

            if need_create:
                self.logger.info("Create setup_data.log table")
                cur.execute(
                    """
                    CREATE TABLE setup_data.log(
                        root_dir text NOT NULL,
                        version text NOT NULL,
                        install_date timestamp NOT NULL,
                        CONSTRAINT mapping_id_pkey PRIMARY KEY (root_dir, version)
                    );"""
                )
        return need_create

    def process_version_dir(self):
        last_installed_version_data = self.get_last_installed_version_data()
        last_installed_version_module = self.get_last_installed_version_module()

        version_dirs = self.list_and_parse_version_dir()

        parsed_version_dirs = list(version_dirs.keys())
        parsed_version_dirs.sort()

        for parsed_version in parsed_version_dirs:
            if (
                parsed_version > last_installed_version_data
                and parsed_version <= last_installed_version_module
            ):
                self.setup_data(version_dirs[parsed_version])

    def list_and_parse_version_dir(self):
        lst_dirs = os.listdir(self.get_option("-d", "--dir"))

        version_dirs = {}

        for dir in lst_dirs:
            if os.path.isdir(os.path.join(self.get_option("-d", "--dir"), dir)):
                version_dirs[tools.parse_version(dir)] = dir

        return version_dirs

    def get_last_installed_version_data(self):
        with self.cur.connection.cursor() as cur:
            cur.execute(
                "SELECT version FROM setup_data.log WHERE root_dir = %s;",
                (self.get_option("-d", "--dir"),),
            )

            lst_installed_version = [
                tools.parse_version(row[0]) for row in cur.fetchall()
            ]
            lst_installed_version.sort(reverse=True)

        if not lst_installed_version:
            lst_installed_version = [tools.parse_version("0.0.0")]
        return lst_installed_version[0]

    def get_last_installed_version_module(self):
        for module_name in os.listdir(
            os.path.join(self.get_option("-d", "--dir"), "..", "modules")
        ):
            module = self.env["ir.module.module"].search([("name", "=", module_name)])
            if module:
                break

        return tools.parse_version(module.latest_version)

    def setup_data(self, version):

        directory = os.path.join(self.get_option("-d", "--dir"), version)

        os.chdir(directory)

        mod = SourceFileLoader(
            "setup_data",
            os.path.join(directory, "setup_data.py"),
        ).load_module()

        self.logger.info(
            f"========== Start setup: {self.get_option('-m', '--module')} {version} =========="
        )

        setup_test_data = mod.SetupData(self.get_option("-m", "--module"))
        setup_test_data.run(self.cur, self.env)

        self.logger.info(
            f"********** End setup: {self.get_option('-m', '--module')} {version} **********"
        )

        self.cur.execute(
            """
            INSERT INTO setup_data.log(root_dir, version, install_date)
            VALUES (%s, %s, %s)
            """,
            (self.get_option("-d", "--dir"), version, datetime.now()),
        )


if __name__ == "__main__":

    SETUP_TEST_DATA = SetupData()
    SETUP_TEST_DATA.run_in_odoo_context(context={"lang": "en_US", "tz": False})
