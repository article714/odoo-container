#!/usr/bin/env python3

"""
©2023 TekFor
License: L-GPL v3

@author: D. Couppé, Apside

This script set data in odoo with a specific module name using XML and CSV files
"""

import os
from abc import abstractmethod

import odoo.tools as tools

from odootools.src.odootools.odooscript import AbstractOdooScript


class AbstractSetupData(AbstractOdooScript):
    def __init__(self, module_name):
        super().__init__()

        self.module_name = module_name

    def run(self, cur, env):
        self.env = env
        self.cur = cur

        self._setup_data()

    @abstractmethod
    def _setup_data(self):
        """
        Re-implement to setup data
        """

    def _loading_data_file(self, file_path):
        self.logger.info("Loading : %s", file_path)

        with open(file_path, "rb") as fp:
            ext = os.path.splitext(file_path)[-1]
            if ext == ".csv":
                tools.convert_csv_import(
                    self.cur, self.module_name, file_path, fp.read(), None
                )
            elif ext == ".xml":
                tools.convert_xml_import(self.cur, self.module_name, fp, None)
            else:
                self.logger.critical("Extension '%s' is not supported", ext)
                raise ValueError("Extension '{}' is not supported".format(ext))

    def _apply_settings(self, settings, company_id=False):
        if company_id:
            self.env.user.company_id = company_id
            settings["company_id"] = company_id.id
        res_config_settings = self.env["res.config.settings"]
        for record in res_config_settings.search([], limit=1):
            record.write(settings)
            break
        else:
            record = res_config_settings.create(settings)

        record.execute()
