ARG DEBIAN_CONTAINER_VERSION
ARG BASE_DEBIAN_VERSION
FROM article714/debian-based-container:${BASE_DEBIAN_VERSION}-${DEBIAN_CONTAINER_VERSION}
LABEL maintainer="C. Guychard <christophe@article714.org>"

ARG BASE_DEBIAN_VERSION
ARG PRODUCT_VERSION
ARG WKHTML_VERSION
ARG WITH_DEBIAN_PYTHON_DEPS
ARG ODOO_PACKAGE_RELEASE


# Container tooling

COPY container /container
COPY dependencies/ /container/dependencies

ENV PATH=/usr/local/bin:${PATH}

# Build container

RUN /container/build.sh

# Expose Odoo services
EXPOSE 8069 8071

# Set the default config file
ENV ODOO_RC /container/config/odoo/odoo.conf
ENV ODOO_RELEASE=${PRODUCT_VERSION}

