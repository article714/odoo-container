# Versions 23.6.0

- added support for odoo 13.0

# Versions 23.5.0

- added modes
- added tools for auto-upgrade
- added tools for setup-data
- added tools for filestore managment
- added tools for extra fonts
- added tools for odoo database managment
- added documentation

# Versions 23.2.0

- added service [redis_workers_pool](container/services/redis_workers_pool/README.md) (issue #14)

# Versions 22.12.0

- added more fonts
- fixed issue #8, warning about a missing font
- added a new tool to test Postresql database existence
- fixed rules to output Odoo logs to odoo-server.log (rsyslog)
- deprecated Odoo 11.0 support (issue #13)
- moved to debian-based-container version 0.11.0 (issue #12)
- improved addons_path to better deal with missing odoo.conf file
- added support for odoo 15.0 & 16.0

# Version 22.10.1

- fixes issue #10 about ODOO_RELEASE

# Version 22.10.0

- fixes urllib dependency for odoo 12.0
- clone_depencies does not upgrade already installed packages by default, as it generates issues.
- fixed issue #2 (container not idempotent for different Odoo Version)
- added ODOO_RELEASE as an environment variable for image
- fixed issue #7 (ODOO_RELEASE used by clone_dependencies.py)
- use same modules for all odoo
- added tooling (provided by Tekfor<https://www.tekfor.cloud>) to automatically update addons_path in odoo.conf on container start
- evolved `build.sh`script so that Odoo can be installed either from source code or debian packages
- update wkhtml2pdf version to 0.12.6 for everyone
- upgrade python version to 3.9.14 for odoo 12.0 and to 3.10 for odoo 14.0
- updated mariadb/mysql dependency for odoo 12.0 & 14.0 based images + purge development packages after install
- changed versioning scheme
- refactored pipeline structure to build image for every supported versions of Odoo in a single pass
- added some tests

# Version 12.0.8

- Fix PyPDF2 version to 1._ for incompatibility between odoo 12.0 and PyPDF2 2._

# Version 12.0.7

- use latest version of PyPDF2 (performance improvements)

# Version 12.0.6

- added some more fonts
- replace wkhtmltopdf (debian) with wkhtmltox (memory issues)
- update base image to debian-based-container:0.9.0
