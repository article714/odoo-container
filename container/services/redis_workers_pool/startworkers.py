import getopt
import json
import os
import signal
import sys
import yaml
import logging
from logging import config as logconfig
from multiprocessing import Process
from time import sleep

from redis import Redis
from redis import exceptions as redisexc
from rq import Queue, SimpleWorker
from rq.job import Job
from rq.defaults import (
    DEFAULT_RESULT_TTL,
    DEFAULT_WORKER_TTL,
    DEFAULT_JOB_MONITORING_INTERVAL,
)
from setproctitle import setproctitle  # pylint: disable=no-name-in-module

from odoo.tools import config as odoo_config
import odoo

# -------------------------------------------------------------------------
# globals
#
# we need this because it wil be used accross processes
#       RUNNER Will be actually set at end of file
#
RUNNER = None
# -------------------------------------------------------------------------


class CustomJob(Job):
    """
    Custome Job Class providing an access to workers context and configuration data
    """

    def __init__(  # pylint: disable=redefined-builtin
        self, id=None, connection=None, serializer=None
    ):
        """
        Add specific properties for
        """
        super().__init__(id=id, connection=connection, serializer=serializer)
        self.config = RUNNER.config
        self.logger = logging.getLogger("RedisWorker")
        self.queue_pool = None

    def get_queue(self, queue_name):
        return self.queue_pool.get_queue(queue_name)


# -------------------------------------------------------------------------
class CustomWorker(SimpleWorker):
    """
    Custom Worker with config support
    """

    def __init__(  # pylint: disable=too-many-arguments
        self,
        queues,
        name=None,
        default_result_ttl=DEFAULT_RESULT_TTL,
        connection=None,
        exc_handler=None,
        exception_handlers=None,
        default_worker_ttl=DEFAULT_WORKER_TTL,
        job_class=None,
        queue_class=None,
        log_job_description=True,
        job_monitoring_interval=DEFAULT_JOB_MONITORING_INTERVAL,
        disable_default_exception_handler=False,
        prepare_for_work=True,
        serializer=None,
    ):
        """
        Add a parameter to default __init__
        """
        super().__init__(
            queues,
            name=name,
            default_result_ttl=default_result_ttl,
            connection=connection,
            exc_handler=exc_handler,
            exception_handlers=exception_handlers,
            default_worker_ttl=default_worker_ttl,
            job_class=job_class,
            queue_class=queue_class,
            log_job_description=log_job_description,
            job_monitoring_interval=job_monitoring_interval,
            disable_default_exception_handler=disable_default_exception_handler,
            prepare_for_work=prepare_for_work,
            serializer=serializer,
        )

        self.logger = logging.getLogger("RedisWorker")
        self.config = RUNNER.config
        self.registry = None
        self.queue_pool = RedisQueuePool(connection=self.connection)

    def work(self, *args, **kwargs):
        self.configure_odoo_server()
        return super().work(*args, **kwargs)

    def execute_job(self, job, queue):
        user_id = job.kwargs.get("user_id", odoo.SUPERUSER_ID)
        job.queue_pool = self.queue_pool

        if float(odoo.release.version.split("-")[0]) < 15.0:
            odoo.api.Environment.reset()

        self.registry.clear_caches()
        with self.registry.cursor() as cur:
            ctx = odoo.api.Environment(cur, user_id, {})["res.users"].context_get()
            env = odoo.api.Environment(cur, user_id, ctx)
            job.kwargs["cur"] = cur
            job.kwargs["env"] = env
            super().execute_job(job, queue)

    def configure_odoo_server(self):
        odoo_config.parse_config(["-c{}".format(os.environ["ODOO_RC"])])
        odoo.cli.server.report_configuration()
        self.registry = odoo.registry(odoo_config.get("db_name"))


# -------------------------------------------------------------------------


class RedisQueuePool:
    def __init__(self, connection):
        self.connection = connection
        self.queues = {}

    def get_queue(self, queue_name):
        return self.queues.setdefault(
            queue_name, Queue(queue_name, connection=self.connection)
        )


# -------------------------------------------------------------------------
class WorkersRunner:
    """
    Config and run RQ workers to process background jobs
    """

    def __init__(self):
        """
        Setup Runner
        """
        self.config = None
        self.configfile = None
        self.logger = None
        self.log_handlers = []
        self.workers = {}

    # *************************************************************
    def print_help(self):  # pylint: disable=no-self-use
        """
        Print help message when required parameter is missing
        or when -h/--help is provided
        """
        help_message = "USAGE : \n\t python3 -m startworkers -c <configfile>"
        help_message += "OPTIONS: \n\n"
        help_message += "-h, --help\t\tShow this message\n"
        help_message += "-c, --config\t\t[REQUIRED] Path to conf file\n"

        return help_message

    def _parse_args(self):
        """
        parse Command line args
        """
        try:
            opts, unneededargs = getopt.getopt(sys.argv[1:], "hc:", ["config="])
        except getopt.GetoptError:
            self.print_help()
            sys.exit(2)

        for opt, arg in opts:
            if opt == "-h":
                self.print_help()
                sys.exit()
            elif opt in ("-c", "--config"):
                self.configfile = arg
        for arg in unneededargs:
            logging.warning("un-nedeed argument %s", str(arg))

    def _set_config(self):
        """
        Parse config file if exists
        """

        if not self.configfile or not os.path.exists(self.configfile):
            logging.error(
                "NO config file found \n\tUSAGE : \n\t\t python3 "
                "-m startworkers -c <configfile>"
            )
            sys.exit(1)

        with open(self.configfile, "r") as f:
            self.config = yaml.safe_load(f)

        if not self.config:
            self.config = {}

        self._set_logging()
        self._add_extra_path()

    def _add_extra_path(self):
        addons_path = self.config.get("extra_python_path", "")
        sys.path.extend(addons_path.split(","))

    def _set_logging(self):
        """
        set logging configuration
        """
        config_file = self.config.get("logging", {}).get(
            "configfile", "/container/config/redis_workers_pool/workers-logging.json"
        )

        if self.logger is not None:
            # reset logging configuration
            for handlr in self.log_handlers:
                self.logger.removeHandler(handlr)
                handlr.close()
            self.log_handlers = []

        logging.basicConfig(
            level=logging.INFO,
            format="%(relativeCreated)6d %(threadName)s %(message)s",
        )

        if os.path.exists(config_file):
            with open(config_file, "r") as config:
                _logging_config = json.load(config)
        else:
            _logging_config = {
                "version": 1,
                "disable_existing_loggers": False,
                "handlers": {
                    "file": {
                        "level": "DEBUG",
                        "class": "logging.FileHandler",
                        "filename": "runner-debug.log",
                    },
                },
                "loggers": {
                    "RedisWorkerRunner": {
                        "handlers": ["file"],
                        "level": "DEBUG",
                        "propagate": True,
                    },
                },
            }

        logconfig.dictConfig(_logging_config)

        # Logging configuration
        self.logger = logging.getLogger("RedisWorkerRunner")

    def _create_pool(self, poolname, queuename, instances=0):
        """
        Starts <instances> runners for pool named <poolname>
        """

        def runtime_func(wname, qname):
            """
            main runtime function for worker
            """
            setproctitle(wname)

            running = False
            while not running:
                redclient = Redis(
                    host=self.config.get("redis", {}).get("host", "localhost"),
                    port=self.config.get("redis", {}).get("port", 6380),
                    db=0,
                )

                queue = Queue(qname, connection=redclient)
                worker = CustomWorker(
                    [queue], connection=redclient, name=wname, job_class=CustomJob
                )
                try:
                    running = True
                    worker.work()
                except redisexc.ConnectionError as err:
                    self.logger.error("Not able to connect to redis server :%s", err)
                    running = False
                    sleep(30)

        wpool = []

        self.logger.warning(
            "Will start %s runners for pool %s on queue %s",
            instances,
            poolname,
            queuename,
        )
        pnum = instances
        while pnum > 0:
            wname = f"redis_wrkr_{poolname}_{pnum}"
            wpool.append(
                Process(
                    target=runtime_func,
                    args=(wname, queuename),
                    name=wname,
                    daemon=True,
                )
            )
            pnum -= 1

        return wpool

    def _create_workers(self):
        """
        create all workers
        """

        # Look for runners to start
        pool_names = self.config.get("pools", {}).keys()
        if pool_names:
            for pool_name in pool_names:
                wrk_nb = self.config["pools"][pool_name]["instances"]
                self.logger.info(" Pool %s has %s instance(s)", pool_name)
                queuename = self.config["pools"][pool_name]["queue_name"]
                self.workers[pool_name] = self._create_pool(
                    pool_name, queuename, wrk_nb
                )
        else:
            self.logger.warning("No pool defined. Starting: default")
            self.workers["default"] = self._create_pool("default", "default", 1)

    def start_it_all(self):
        """
        start all runners
        """
        # Load config
        if self.configfile is None:
            self._parse_args()
        if self.config is None:
            self._set_config()

        self._create_workers()

        # Start pools
        for pool_name in self.workers:
            self.logger.warning("Starting Pool %s", pool_name)
            for proc in self.workers[pool_name]:
                proc.start()

    def stop_it_all(self):
        """
        stop all processes
        """
        # Wait for stop
        for pool_name in self.workers:
            self.logger.warning("Stopping Pool %s", pool_name)
            for proc in self.workers[pool_name]:
                proc.terminate()

    def wait_for_stop(self):
        """
        Waiting for all child process to be terminated
        """
        for pool_name in self.workers:
            for proc in self.workers[pool_name]:
                proc.join()
            self.logger.warning("All workers stopped in all Pools %s", pool_name)


# -------------------------------------------------------------------------
def main():
    """
    main function that configures and start worker(s)
    """

    RUNNER.start_it_all()

    # register signal handlers
    def kill_handler(signum, frame):  # pylint: disable=unused-argument
        logging.warning("Redis Workers: stopping everything")
        RUNNER.stop_it_all()

    def hup(signum, frame):  # pylint: disable=unused-argument
        logging.warning("Redis Workers: receive SIGHUP, do nothing")

    signal.signal(signal.SIGTERM, kill_handler)
    signal.signal(signal.SIGINT, kill_handler)
    signal.signal(signal.SIGHUP, hup)

    RUNNER.wait_for_stop()


# -------------------------------------------------------------------------
#
# initialize RUNNER
#
RUNNER = WorkersRunner()

# -------------------------------------------------------------------------
if __name__ == "__main__":
    # Go and start main
    main()
