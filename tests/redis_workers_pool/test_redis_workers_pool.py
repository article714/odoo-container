import time
import sys
from redis import Redis
from rq import Queue, get_current_job


def get_main_company_name(*args, **kwargs):
    env = kwargs["env"]
    rec = env.ref("base.main_company")
    return rec.name


def long_job(*args, **kwargs):
    time.sleep(kwargs.get("wait", 1))


def join_job(job, expected_status, max_loop):
    loop = 0
    while job.get_status(refresh=True) not in expected_status and loop < max_loop:
        time.sleep(1)
        loop += 1


def test_env_odoo():
    print("================= test_env_odoo ===============")
    conn = Redis(port=6380, host="localhost")
    queue = Queue("test_1", connection=conn)

    job = queue.enqueue("test_redis_workers_pool.get_main_company_name")

    join_job(job, ("finished", "failed"), 20)

    expected_value = "YourCompany"
    return_value = job.return_value

    print(f"Job satus: {job.get_status()}")
    print(f"Return value : {return_value}")

    if return_value != expected_value:
        raise ValueError(
            f"Error on test_env_odoo. Expected value {expected_value} got {return_value}"
        )


def test_multi_job_on_same_chanel():
    print("================= test_multi_job_on_same_chanel ===============")

    def check_job_status(job, status):
        if job.get_status(refresh=True) != status:
            raise ValueError(
                f"Expected job status is {status} but is {job.get_status()}"
            )

    conn = Redis(port=6380, host="localhost")
    queue = Queue("test_3", connection=conn)

    job_10 = queue.enqueue("test_redis_workers_pool.long_job", wait=10)
    job_4 = queue.enqueue("test_redis_workers_pool.long_job", wait=4)
    job_2 = queue.enqueue("test_redis_workers_pool.long_job", wait=2)
    job_1 = queue.enqueue("test_redis_workers_pool.long_job", wait=1)

    join_job(job_2, ["started"], 20)

    check_job_status(job_10, "started")
    check_job_status(job_4, "started")
    check_job_status(job_2, "started")
    check_job_status(job_1, "queued")
    print("check 1 ok")

    join_job(job_4, ["finished"], 5)
    check_job_status(job_10, "started")
    check_job_status(job_4, "finished")
    check_job_status(job_2, "finished")
    check_job_status(job_1, "finished")
    print("check 2 ok")

    join_job(job_10, ["finished"], 11)
    check_job_status(job_10, "finished")
    check_job_status(job_4, "finished")
    check_job_status(job_2, "finished")
    check_job_status(job_1, "finished")
    print("check 3 ok")


def main_test():
    test_env_odoo()
    test_multi_job_on_same_chanel()


if __name__ == "__main__":
    main_test()
