#!/bin/bash

# set -x

#-------------------
# test args

if [ "${1}" = "" ]; then
	echo "no env file provided"
else
	if [ -f ${1} ]; then
		source ${1}
	fi
fi

CUR_DIR=`pwd`

#-------------------
# removing odoo Database

echo "-- Removing Odoo Database on ${db_host}, named ${db_name}"

res=$(psql  postgresql://$db_host:$db_port/postgres?user=$db_username\&password=$db_password -c "select pg_terminate_backend(pid) from pg_stat_activity where datname = '${db_name}';" 2>&1)
res=$(psql  postgresql://$db_host:$db_port/postgres?user=$db_username\&password=$db_password -c "drop database \"${db_name}\";" 2>&1)

echo $res

if [ "${res}" = "DROP DATABASE" ]
then
	ERR_CODE=0
elif [ "${res}" = "ERROR:  database \"${db_name}\" does not exist" ]
then
	ERR_CODE=0
else
	ERR_CODE=1
fi

exit $ERR_CODE
